import React from 'react';
import { Link, Routes, Route } from 'react-router-dom';

class PostsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
    };
  }

  async componentDidMount() {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts');
    const posts = await response.json();
    this.setState({ posts });
  }

  render() {
    return this.state.posts.map((post) => (
      <Link to={`/posts/${post.id}`}>{post.title}</Link>
    ));
  }
}

export default PostsPage;
