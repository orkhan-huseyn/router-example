import React from 'react';
import { Link, Routes, Route } from 'react-router-dom';
import './App.css';

import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import PostsPage from './pages/PostsPage';

class App extends React.Component {
  render() {
    return (
      <header>
        <nav>
          <Link to="/home">Home</Link>
          <Link to="/about">About</Link>
          <Link to="/posts">Posts</Link>
        </nav>
        <main>
          <Routes>
            <Route path="/home" element={<HomePage />} />
            <Route path="/about" element={<AboutPage />} />
            <Route path="/posts" element={<PostsPage />} />
            <Route path="/posts/:id" element={<h1>Post page</h1>} />
            <Route path="*" element={<h1>404 Not Found</h1>} />
          </Routes>
        </main>
      </header>
    );
  }
}

export default App;
